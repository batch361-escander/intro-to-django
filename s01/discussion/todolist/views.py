from django.forms.models import model_to_dict
from django.shortcuts import render, redirect
from django.http import HttpResponse
# from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

#Local imports
from .models import ToDoItem

# Create your views here.
def index(request):
    todoitem_list = ToDoItem.objects.all()
    context = {'todoitem_list': todoitem_list }
    return render(request, "todolist/index.html", context)
    
def todoitem(request, todoitem_id):
    todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))
    return render(request, "todolist/todoitem.html", todoitem)
    # response = "You are viewing the details of %s"
    # return HttpResponse(response % todoitem_id)

def register(request):
    users = User.objects.all()

    is_user_registered = False
    context = {
        "is_user_registered": is_user_registered
    }

    for indiv_user in users:
        if indiv_user.username == "johndoe":
            is_user_registered = True
            break

    if is_user_registered == False:
        user_to_register = User()
        user_to_register.username = "johndoe"
        user_to_register.first_name = "John"
        user_to_register.last_name = "Doe"
        user_to_register.email = "john@mail.com"

        user_to_register.set_password("john1234")
        user_to_register.is_staff = False
        user_to_register.is_active = True

        user_to_register.save()

        context = {
            "first_name": user_to_register.first_name,
            "last_name": user_to_register.last_name
        }

    return render(request, "todolist/register.html", context)

def change_password(request):

    is_user_authenticated = False
    user = authenticate(username ="johndoe", password = "johndoe1")
    print(user)
    if user is not None:
        authenticated_user = User.objects.get(username='johndoe')
        authenticated_user.set_password("johndoe1")
        authenticated_user.save()
        is_user_authenticated = True

    context = {
        "is_user_authenticated" : is_user_authenticated
        }
    
    return render(request, "todolist/change_password.html", context)


def login_view(request):
    username = "johndoe"
    password = "johndoe1"
    user = authenticate(username=username, password=password)
    context = {
        "is_user_authenticated": False
    }
    print(user)
    if user is not None:
        # Saves the user’s ID in the session using Django's session framework
        # - Updates the user's authentication status, making them an authenticated user for the current session
        login(request, user)
        return redirect("index")
    else:
        return render(request, "todolist/login.html", context)

def logout_view(request):
    logout(request)
    return redirect("index")